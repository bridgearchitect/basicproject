#include <stdio.h>

/* entrance point */
int main() {

    /* decleration of two numbers */
    int a, b;

    /* read two numbers */
    printf("Read number a:\n");
    scanf("%d", &a);
    printf("Read number b:\n");
    scanf("%d", &b);

    /* add two numbers */
    int c = a + b;

    /* print information about sum */
    printf("Sum of two numbers: %d\n", c);


}
